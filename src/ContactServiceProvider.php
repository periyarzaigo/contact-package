<?php

namespace Zaigo\Contact;

use Illuminate\Support\ServiceProvider;


class ContactServiceProvider extends ServiceProvider
{
    /**
     * Boot the service provider.
     *
     * @return void
     */
    public function boot()
    {
        $this->LoadRoutesFrom(__DIR__.'/routes/web.php');
		$this->LoadViewsFrom(__DIR__.'/views','contact');
		$this->LoadMigrationsFrom(__DIR__.'/database/migrations');
		$this->mergeConfigFrom(__DIR__.'/config/contact.php','contact');
		$this->Publishes([__DIR__.'/config/contact.php'=>config_path('contact.php')]);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
       
    }

   
}
