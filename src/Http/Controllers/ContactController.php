<?php

namespace Zaigo\Contact\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
Use Zaigo\Contact\Models\Contact;
Use Zaigo\Contact\Mail\ContactMailable;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    public function index()
	{
		return view('contact::contact');
	}
	public function send(Request $request)
	{
		Mail::to(config('contact.send_to_email'))->send(new ContactMailable($request->message,$request->name));
		Contact::create($request->all());
		return redirect(route('contact'));
		
	}
}
